package org.eiji;

import java.util.*;

public class Fibonacci {

	public static int compute(int input) {
		// { 0, 0 }, { 1, 1 }, { 2, 1 },
//        { 3, 2 }, { 4, 3 }, { 5, 5 }, { 6, 8 } }
//		0 1 1 2 3 5 8 
		int first = 0;
		int second = 1;
		int next = 0;
		
		for (int fibCount = 0 ; fibCount < input + 1 ; fibCount++){
			if (fibCount <= 1) {
				next = fibCount;
			} else {
				next = first + second;
				first = second;
				second = next;
			}
//			System.out.println(next);
			
		}
		System.out.println("input:" + input + ", result:" + next);
		return next;
	}

}
