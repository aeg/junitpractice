package org.eiji;

import java.util.logging.*;


public class SpyExample {
	public Logger logger = Logger.getLogger(SpyExample.class.getName());
	
	public void doSomething() {
		logger.info("doSomething");
	}
}
