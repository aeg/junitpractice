package org.eiji.mock;

import java.util.*;

public class MethodExtractExample {
	Date date = newDate();
	
	public void doSomething() {
		this.date = newDate();
	}
	
	Date newDate() {
		return new Date();
	}

}
