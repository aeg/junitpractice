package org.eiji.mock;

public interface RandomNumberGenerator {
	int nextInt();
}
