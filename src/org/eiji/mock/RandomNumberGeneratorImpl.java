package org.eiji.mock;

import java.util.*;

public class RandomNumberGeneratorImpl implements RandomNumberGenerator {

	private final Random rand = new Random();
	@Override
	public int nextInt() {
		return rand.nextInt();
	}

}
