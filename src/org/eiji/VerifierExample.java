package org.eiji;

public class VerifierExample {

	protected int value;

	public void set(int setValue) {
		this.value = setValue;		
	}

	public void add(int value) {
		this.value += value;
	}

	public void minus(int value){
		this.value -= value;
	}

	public int getValue() {
		return value;
	}

}
