package org.eiji;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import org.junit.*;
import org.junit.rules.*;

public class VerifierExampleTest {

  @Rule
  public Verifier verifier = new Verifier() {
    protected void verify() throws Throwable {
      assertThat("value should be 0.", sut.value, is(0));

    }
  };
  VerifierExample sut;

  @Before
  public void setUp() throws Exception {
    sut = new VerifierExample();
  }

  @After
  public void tearDown() throws Exception {
    // do nothing
  }

  @Test
  public void getValueで計算結果を取得する() throws Exception {
    sut.set(200);
    sut.add(100);
    sut.minus(50);
    int actual = sut.getValue();
    assertThat(actual, is(250));
    sut.minus(250);
  }

}
