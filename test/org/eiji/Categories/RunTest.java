package org.eiji.Categories;

import org.junit.experimental.categories.Categories;
import org.junit.experimental.categories.Category;
import org.junit.experimental.categories.Categories.ExcludeCategory;
import org.junit.runner.RunWith;
import org.junit.runners.Suite.SuiteClasses;
import org.testng.annotations.Test;




public class RunTest {

  @Category(FirstTest.class)
  @Test
  public void a() {
    System.out.println("testA");
  }

  @Category(SlowTest.class)
  @Test
  public void b() {
    System.out.println("testB");
  }
}