package org.eiji;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.junit.Assert.*;

import org.junit.*;
import org.junit.rules.*;
import org.junit.runner.*;
import org.junit.experimental.runners.Enclosed;

@RunWith(Enclosed.class)
public class ShopInfoTest {

	public static class InstancedTest {
		
		@Rule
		public ErrorCollector ec = new ErrorCollector();
		@Test
		public void デフォルトコンストラクタ() throws Exception {
			// Exercise
			ShopInfo instance = new ShopInfo();
			// Verify
			ec.checkThat(instance, is(notNullValue()));
			ec.checkThat(instance.id, is(nullValue()));
			ec.checkThat(instance.name, is(""));
			ec.checkThat(instance.address, is(""));
			ec.checkThat(instance.url,  is(nullValue()));
			
		}
	}
}
