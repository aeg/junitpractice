package org.eiji.mock;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

import java.util.*;
import java.util.concurrent.atomic.*;

import org.junit.*;

public class RandomMockTest {

	@Test
	public void choiceでAを返す() {
		List<String> options = new ArrayList<String>();
		options.add("A");
		options.add("B");
		Randoms sut = new Randoms();
		// モックの設定
		final AtomicBoolean isCallNextIntMethod = new AtomicBoolean(false);
		sut.generator = new RandomNumberGenerator() {
			@Override
			public int nextInt() {
				isCallNextIntMethod.set(true);
				return 0;
			}
		};
		assertThat(sut.choice(options),is("A"));
		assertThat(isCallNextIntMethod.get(), is(true));
	}

}
