package org.eiji.mock;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.junit.Assert.*;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.*;

import java.util.*;
import java.util.logging.*;

import org.eiji.*;
import org.junit.*;
import org.mockito.*;
import org.mockito.invocation.*;
import org.mockito.stubbing.*;

public class MockExampleTest {

	@Test
	public void モックオブジェクトのテスト() {
		List<String> mock = Mockito.mock(List.class);
		assertThat(mock.get(0),is(nullValue()));
		assertThat(mock.contains("Hello"),is(false));
		
	}
	
	@Test
	public void モックオブジェクトのテスト2() {
		List<String> stub = mock(List.class);
		when(stub.get(0)).thenReturn("Hello");
		assertThat(stub.get(0), is("Hello"));
	}
	
	@Test
	public void モックオブジェクトのテスト3() {
		List<String> stub = mock(List.class);
		when(stub.get(0)).thenReturn("Hello");
		when(stub.get(1)).thenReturn("World");
		when(stub.get(2)).thenThrow(new IndexOutOfBoundsException());
		assertThat(stub.get(1), is("World")); // 例外が送出される
	}
	
	@Test(expected = RuntimeException.class)
	public void void型を返すスタブメソッドのテスト() {
		List<String> stub = mock(List.class);
		doThrow(new RuntimeException()).when(stub).clear();
		stub.clear();
	}
	
	@Test
	public void 任意の整数に対するスタブメソッドのテスト() throws Exception {
		List<String> stub = mock(List.class);
		when(stub .get(anyInt())).thenReturn("Hello");
		assertThat(stub.get(0),is("Hello"));
		assertThat(stub.get(1),is("Hello"));
		assertThat(stub.get(999),is("Hello"));
	}
	
	@Test
	public void スタブメソッドの検証() throws Exception {
		List<String> mock = mock(List.class);
		mock.clear();
		mock.add("Hello");
		mock.add("Hello");
		verify(mock).clear();
		verify(mock, times(2)).add("Hello");
		verify(mock, never()).add("World");
	}
	
	@Test
	public void 部分的なモックオブジェクトのテスト() throws Exception {
		List list = new ArrayList();
		List spy = spy(list);
		when(spy.size()).thenReturn(100);
//		when(spy.get(0)).thenReturn("World");
		spy.add("Hello");

	//	assertThat(spy.get(0), is("Hello"));
		assertThat(spy.size(), is(100));
	}

	@Test
	public void スパイオブジェクト作成のテスト() throws Exception {
		List<String> list = new LinkedList<String>();
		List<String> spy = spy(list);
		doReturn("Mockito").when(spy).get(1);
		spy.add("Hello");
		spy.add("World");
		verify(spy).add("Hello");
		verify(spy).add("World");
		assertThat(spy.get(0),is("Hello"));
		assertThat(spy.get(1),is("Mockito"));
	}
	
	@Test
	public void ロガーのスパイオブジェクトを利用したメソッド() throws Exception {
		//Setup
		SpyExample sut = new SpyExample();
		Logger spy = spy(sut.logger);
		final StringBuilder infoLog = new StringBuilder();
		doAnswer(new Answer<Void>() {
			
			@Override
			public Void answer(InvocationOnMock invocation) throws Throwable {
				infoLog.append(invocation.getArguments()[0]);
				invocation.callRealMethod();
				return null;
			}
		}).when(spy).info(anyString());
		sut.logger = spy;
		
		//Exercise
		sut.doSomething();
		
		//Verify
		assertThat(infoLog.toString(),is("doSomething"));
	}
}
