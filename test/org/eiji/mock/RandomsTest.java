package org.eiji.mock;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

import java.util.*;

import org.junit.*;

public class RandomsTest {

	@Test
	public void choiceでAを返す() {
		List<String> options = new ArrayList<String>();
		options.add("A");
		options.add("B");
		Randoms sut = new Randoms();
		// スタブの設定
		sut.generator = new RandomNumberGenerator() {
			
			@Override
			public int nextInt() {
				return 0;
			}
		};
		assertThat(sut.choice(options), is("A"));
		
		
	}

}
