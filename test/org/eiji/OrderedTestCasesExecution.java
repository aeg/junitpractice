package org.eiji;

import org.junit.*;
import org.junit.runners.MethodSorters;

//Running test cases in order of method names in ascending order
//MethodSorters.JVM: the order in which the methods are returned by the JVM, potentially different order each time
//MethodSorters.DEFAULT: deterministic ordering based upon the hashCode
//MethodSorters.NAME_ASCENDING: order based upon the lexicographic ordering of the names of the tests


@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class OrderedTestCasesExecution {
    @Test
    public void secondTest() {
        System.out.println("Executing second test");
    }
     
    @Test
    public void firstTest() {
        System.out.println("Executing first test");
    }
 
    @Test
    public void thirdTest() {
        System.out.println("Executing third test");
    }

    @Test
    public void a01Test() {
        System.out.println("Executing a01 test");
    }
}
