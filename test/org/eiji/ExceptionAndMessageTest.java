package org.eiji;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;

import org.junit.*;

public class ExceptionAndMessageTest {

  @Test
  public void 例外の発生とメッセージを検証する() throws Exception {
    try {
      throwNewIllegalArgumentException();
      fail("例外が発生しない");
    } catch (IllegalArgumentException e) {
      assertThat(e.getMessage(), is("argument is null."));
    }
  }

  private void throwNewIllegalArgumentException() throws Exception {
    throw new IllegalArgumentException("argument is null.");
  }
}
