package org.eiji;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

import org.junit.*;
import org.junit.experimental.theories.*;
import org.junit.runner.*;
import static org.junit.Assume.*;

@RunWith(Theories.class)
public class MemberTest {

	@DataPoints
	public static int[] AGES = {15, 20, 25, 30};
	
	@DataPoints
	public static Gender[] GENDERS = Gender.values();
	
	@Theory
	public void canEntryは25歳以下の女性の場合にtrueを返す(
			int age, Gender gender) throws Exception {
		System.out.println("test: gernder = " + gender + ", age = " + age );
		assumeTrue(age <= 25 & gender == Gender.FEMALE);
		assertThat(Member.canEntry(age, gender), is(true));
	}
	
	
}
