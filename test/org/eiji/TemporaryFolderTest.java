package org.eiji;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import java.io.File;
import java.io.IOException;

public class TemporaryFolderTest {

  @Rule
  public TemporaryFolder folder = new TemporaryFolder();

  @Test
  public void 一時フォルダ利用のテスト() throws IOException {
    // 事前準備
    File createdFile1 = folder.newFile("myfile.txt");
    File createdFolder = folder.newFolder("subfolder");
    File createdFile2 = folder.newFile("subfolder/myfile2.txt");

    System.out.println(folder.getRoot().getAbsolutePath());
    for (File file : folder.getRoot().listFiles()) {
      System.out.println(file.getName());
    }
    System.out.println("----------");
    for (File file : createdFolder.listFiles()) {
      System.out.println(file.getName());
    }
  }
}
