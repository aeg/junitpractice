package org.eiji.snake;

import junit.framework.TestCase;
import org.junit.Test;
import org.yaml.snakeyaml.Yaml;
import org.yaml.snakeyaml.constructor.Constructor;
import org.yaml.snakeyaml.nodes.MappingNode;
import org.yaml.snakeyaml.nodes.NodeId;

import java.util.List;
import java.util.Map;

/**
 * Example for http://code.google.com/p/snakeyaml/wiki/howto
 */
public class SelectiveConstructorTest extends TestCase {
    class SelectiveConstructor extends Constructor {
        public SelectiveConstructor() {
            // define a custom way to create a mapping node
            yamlClassConstructors.put(NodeId.mapping, new MyPersistentObjectConstruct());
        }

        class MyPersistentObjectConstruct extends ConstructMapping {
            @Override
            protected Object constructJavaBean2ndStep(MappingNode node, Object object) {
                Class<?> type = node.getType();
                if (type.equals(MyPersistentObject.class)) {
                    // create a map
                    Map<Object, Object> map = constructMapping(node);
                    String id = (String) map.get("id");
                    return new MyPersistentObject(id, 17);
                } else {
                    // create JavaBean
                    return super.constructJavaBean2ndStep(node, object);
                }
            }
        }
    }

    @Test
    public void testConstructor() {
        Yaml yaml = new Yaml(new SelectiveConstructor());
//        Yaml yaml = new Yaml(new Constructor());
        List<?> data = (List<?>) yaml
                .load("- 1\n- 2\n- !!org.eiji.snake.MyPersistentObject {amount: 222, id: persistent}");
        System.out.println(data);

        assertEquals(3, data.size());
        MyPersistentObject myObject = (MyPersistentObject) data.get(2);
        assertEquals(17, myObject.getAmount());
        assertEquals("persistent", myObject.getId());
    }

}

class MyPersistentObject {
    private String id;
    private int amount;

    public MyPersistentObject() {
        this.id = "noid";
        this.amount = 222;
    }

    public MyPersistentObject(String id, int amount) {
        this.id = id;
        this.amount = amount;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    @Override
    public String toString() {
        return "MyPersistentObject [id=" + id + ", amount=" + amount + "]";
    }
}

