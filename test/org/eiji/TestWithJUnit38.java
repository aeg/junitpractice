package org.eiji;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import org.junit.runner.*;

@RunWith(value=org.junit.internal.runners.JUnit38ClassRunner.class) 
public class TestWithJUnit38 extends junit.framework.TestCase {
	
	public void testname() throws Exception {
		String a = "test";
		
		
		assertEquals("test", a);
		assertThat(a,is("test"));
	}
}
