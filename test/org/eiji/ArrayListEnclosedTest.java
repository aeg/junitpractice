package org.eiji;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

import java.util.*;

import org.junit.*;
import org.junit.runner.*;
import org.junit.experimental.runners.Enclosed;

@RunWith(Enclosed.class)
public class ArrayListEnclosedTest {
	
	public static class listに1件追加してある場合 {
		private ArrayList<String> sut;
		
		@Before
		public void setUp() throws Exception {
			sut = new ArrayList<String>();
			sut.add("A");
		}
		
		@Test
		public void sizeは1を返す() throws Exception {
			int actual = sut.size();
			assertThat(actual, is(1));
		}
		
	}
	
	public static class listに2件追加してある場合 {
		private ArrayList<String> sut;
		
		@Before
		public void setUp() throws Exception {
			sut = new ArrayList<String>();
			sut.add("A");
			sut.add("B");		}
		
		@Test
		public void sizeは2を返す() throws Exception {
			int actual = sut.size();
			assertThat(actual, is(2));
		}
		
	}
	
	public static class listに3件追加してある場合 {
		private ArrayList<String> sut;
		
		@Before
		public void setUp() throws Exception {
			sut = new ArrayList<String>();
			sut.add("A");
			sut.add("B");		
			sut.add("C");		
		}
		
		@Test
		public void sizeは3を返す() throws Exception {
			int actual = sut.size();
			assertThat(actual, is(3));
		}
		
	}
}
