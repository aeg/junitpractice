package org.eiji;

import static org.junit.Assert.*;

import org.junit.*;
import org.junit.rules.*;

public class BaseTestResource extends ExternalResource{

	@Override
	protected void before() throws Throwable {
		System.out.println("before:");
	}
	
	@Override
	protected void after() {
		System.out.println("after:");
	}
}
