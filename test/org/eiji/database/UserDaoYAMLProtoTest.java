package org.eiji.database;

//import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
//import org.eiji.yaml.YamlDataSetProto;
import org.dbunit.dataset.ITable;
import org.eiji.yaml.YamlDataSetProto;
import org.junit.*;
import org.junit.experimental.runners.Enclosed;
import org.junit.runner.RunWith;
//import org.xml.sax.InputSource;

import java.util.List;

import static org.eiji.database.ITableMatcher.tableOf;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * Created with IntelliJ IDEA.
 * User: aeg
 * Date: 2013/02/21
 * Time: 2:00
 * To change this template use File | Settings | File Templates.
 */
@RunWith(Enclosed.class)
public class UserDaoYAMLProtoTest {

    public static class usersHas2RecordsCase {
        @ClassRule
        public static H2DatabaseServer server = new H2UTDatabaseServer();
        @Rule
        public DBUnitTester tester = new UserDaoDbUnitTester("fixture.yaml");

        UserDao sut;

        @Before
        public void setUp() throws Exception {
            this.sut = new UserDao();
        }

        @Test
        public void getListで2件取得できる事() throws Exception {
            // Excercise
            List<String> actual = sut.getList();
            // Verify
            assertThat(actual, is(notNullValue()));
            assertThat(actual.size(), is(2));
            assertThat(actual.get(0), is("一郎"));
            assertThat(actual.get(1), is("次郎"));

        }

        @Test
        public void insertで1件追加できる() throws Exception{
            // Excercise
            sut.insert("三郎");
            // Verify
            ITable actual = tester.getConnection().createDataSet().getTable("users");

            ITable expected = new YamlDataSetProto("expected.yaml").getTable("users");
            assertThat(actual, is(tableOf(expected)));
        }

        public static class usersHas0RecordsCase {
            @ClassRule
            public static H2DatabaseServer server = new H2UTDatabaseServer();
            @Rule
            public DBUnitTester testr = new UserDaoDbUnitTester("zero_fixtures.yaml");

            UserDao sut;

            @Before
            public void setUp() throws Exception {
                this.sut = new UserDao();
            }


            @Test
            public void getListで0件取得できる事() throws Exception {
                // Exercise
                List<String> actual = sut.getList();
                // Verify
                assertThat(actual, is(notNullValue()));
                assertThat(actual.size(), is(0));
            }
        }


        static class H2UTDatabaseServer extends H2DatabaseServer {
            public H2UTDatabaseServer() {
                super("h2", "db", "ut");
            }
        }

        static class UserDaoDbUnitTester extends DBUnitTester {
            private final String fixture;
            public UserDaoDbUnitTester(String fixture) {
                super("org.h2.Driver", "jdbc:h2:tcp://localhost/db;SCHEMA=ut",
                        "sa", "", "ut");
                this.fixture = fixture;
            }

            @Override
            protected void before() throws Exception {
                executeQuery("DROP TABLE IF EXISTS users");
                executeQuery(
                        "CREATE TABLE users(id INT AUTO_INCREMENT, name VARCHAR(64))"
                );
            }
            @Override
            protected org.dbunit.dataset.IDataSet createDataSet() throws Exception {
                System.out.println("fixture:" + fixture);
                //return new FlatXmlDataSetBuilder().build(getClass().getResourceAsStream(fixture));
//                return new FlatXmlDataSetBuilder().build(new InputSource(fixture));
//                return new YamlDataSetProto(new File(fixture));

                //System.out.println((new YamlDataSetProto(fixture)).getTable("users").getClass());
                System.out.println((new YamlDataSetProto(fixture)).getTable("users"));
                return new YamlDataSetProto(fixture);
            }
        }


    }


}
