package org.eiji.yaml;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: aeg
 * Date: 2013/02/21
 * Time: 22:46
 * To change this template use File | Settings | File Templates.
 */
public class BookStore {

    List<Book> cart = new ArrayList<Book>();

    public void addToCart(Book book, int num) {
        for (int i = 0; i < num; i++) {
            cart.add(book);
        }
    }

    public int getTotalPrice() {
        int result = 0;
        for (Book book : cart) {
            result += book.getPrice();
        }
        return result;
    }

    public Book get(int idx) {
        return cart.get(idx);
    }
}
