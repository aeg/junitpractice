package org.eiji.yaml;

import org.junit.Test;
import org.xml.sax.InputSource;
import org.yaml.snakeyaml.Yaml;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;

import static org.hamcrest.CoreMatchers.sameInstance;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

/**
 * Created with IntelliJ IDEA.
 * User: aeg
 * Date: 2013/02/21
 * Time: 22:41
 * To change this template use File | Settings | File Templates.
 */
public class BookStoreYamlTest {

    @Test
    public void getTotalPrice() throws Exception {
        // SetUp
        BookStore sut = new BookStore();

//        Book book = (Book) new Yaml()
//                .load(getClass().getResourceAsStream("book_fixtures.yaml"));
        InputStream input = new FileInputStream(new File("book_fixtures.yaml"));
        Book book = (Book) new Yaml()
                .load(input);

        sut.addToCart(book, 1);
        // Exercise & Verify
        assertThat(sut.get(0), is(sameInstance(book)));
    }

}
